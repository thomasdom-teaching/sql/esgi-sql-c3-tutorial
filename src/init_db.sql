-- Disconnect from current database and connect to postgres for maintenance
\c postgres

-- Recreate fresh database
DROP DATABASE IF EXISTS sql_c3_tutorial;

CREATE DATABASE sql_c3_tutorial OWNER postgres;

-- Reconnect to current database
\c sql_c3_tutorial

-- Schema
CREATE TABLE can (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    capacity_cl INTEGER NOT NULL CHECK (capacity_cl > 0)
);

CREATE TABLE shop (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL
);

CREATE TABLE student_address (
    id SERIAL PRIMARY KEY,
    address VARCHAR(128) NOT NULL,
    country VARCHAR(64) NOT NULL
);

CREATE TABLE student (
    login VARCHAR(8) PRIMARY KEY,
    firstname VARCHAR(64),
    lastname VARCHAR(64) NOT NULL,
    gender BOOLEAN NOT NULL DEFAULT FALSE, -- 0 if it's a boy, 1 if it's a girl
    address_id INTEGER REFERENCES student_address (id) ON DELETE CASCADE
);

CREATE TABLE shop_can (
    shop_id INTEGER NOT NULL REFERENCES shop (id) ON DELETE CASCADE,
    can_id INTEGER NOT NULL REFERENCES can (id) ON DELETE CASCADE,
    price FLOAT NOT NULL,
    PRIMARY KEY(shop_id, can_id)
);

CREATE TABLE student_can_shop (
    id SERIAL PRIMARY KEY,
    login VARCHAR(8) NOT NULL REFERENCES student (login) ON DELETE CASCADE,
    can_id INTEGER NOT NULL REFERENCES can (id) ON DELETE CASCADE,
    shop_id INTEGER NOT NULL REFERENCES shop (id) ON DELETE CASCADE,
    purchase_time TIMESTAMP NOT NULL
);

-- Data fixtures
INSERT INTO can (name, capacity_cl)
VALUES 
    ('Coke', 25),
    ('Oasis', 25),
    ('Diet Coke', 25),
    ('Orangina', 25),
    ('Sprite', 25),
    ('Ice Tea', 25),
    ('Pepsi', 25),
    ('Water', 50),
    ('Fanta', 25),
    ('Orange Juice', 33);

INSERT INTO student_address (address, country)
VALUES
    ('123 rue Bidon', 'France'),
    ('555 New York avenue', 'United States'),
    ('10 Downing Street', 'United Kingdom');

INSERT INTO student
VALUES
    ('nbarray', 'Nicolas', 'Barray', FALSE, 1),
    ('wbekhtao', 'Walid', 'Bekhtaoui', FALSE, 2),
    ('rbillon', 'Rémi', 'Billon', FALSE, 1),
    ('abitar', NULL, 'Bitar', FALSE, 1),
    ('cbrauge', 'Camille', 'Brauge', TRUE, 3),
    ('qcoelho', 'Quentin', 'Coelho', FALSE, 1),
    ('adavray', 'Arthur', 'D''avray', FALSE, NULL),
    ('pdagues', 'Pierre-Louis', 'Dagues', FALSE, 3),
    ('jdonnett', 'Jean-Baptiste', 'Donnette', FALSE, 1),
    ('agaillar', 'Arnaud', 'Gaillard', FALSE, 2),
    ('rgozlan', 'Rafael', 'Gozlan', FALSE, 2),
    ('lgroux', NULL, 'Groux', FALSE, 3),
    ('aguiho', 'Alexis', 'Guiho', FALSE, NULL),
    ('nlayet', 'Nils', 'Layet', FALSE, 2),
    ('qlhours', 'Quentin', 'L''Hours', FALSE, 2),
    ('llubrano', 'Luc-Junior', 'Lubrano-Lavadera', FALSE, 1),
    ('hmaurer', 'Hugo', 'Maurer', FALSE, 3),
    ('aou', 'ArNAud', 'Ou', FALSE, 3),
    ('spiat', 'Sébastien', 'Piat', FALSE, 1),
    ('tsitum', 'Tania', 'Situm', TRUE, NULL),
    ('ataing', 'Anais', 'Taing', TRUE, 1),
    ('bteixeir', 'Brian', 'Teixeira', FALSE, 1),
    ('gthepaut', 'Guillaume', 'Thepaut', FALSE, 1),
    ('atoubian', 'Adrien', 'Toubiana', FALSE, 3),
    ('ctresarr', NULL, 'Tresarrieu', FALSE, 2),
    ('pveyry', 'Pierre-Alexandre', 'Veyry', FALSE, 3),
    ('fvisoiu', 'Francis', 'Visoiu', FALSE, 3);

INSERT INTO shop (name)
VALUES 
    ('Nation'),
    ('Nation 2'),
    ('Erard'),
    ('Voltaire'),
    ('Beaugrenelle'),
    ('Montsouris'),
    ('Jourdan');

INSERT INTO shop_can (shop_id, can_id, price)
VALUES
    ('1','1','1.51'),
    ('1','2','2.31'),
    ('1','3','4.03'),
    ('1','4','2.1'),
    ('1','5','4.26'),
    ('1','6','4.52'),
    ('1','7','4.88'),
    ('1','8','2.81'),
    ('1','9','1.38'),
    ('1','10','1.62'),
    ('2','1','3.82'),
    ('2','2','3.55'),
    ('2','3','2.71'),
    ('2','4','2.62'),
    ('2','5','4.13'),
    ('2','6','3.92'),
    ('2','7','3.08'),
    ('2','8','4.85'),
    ('2','9','2.91'),
    ('2','10','3.31'),
    ('3','1','4.18'),
    ('3','2','1.64'),
    ('3','3','4.45'),
    ('3','4','3.65'),
    ('3','5','1.88'),
    ('3','6','1.8'),
    ('3','7','1.38'),
    ('3','8','4.49'),
    ('3','9','3.9'),
    ('3','10','2.17'),
    ('4','1','4.28'),
    ('4','2','2.6'),
    ('4','3','4.53'),
    ('4','4','1.32'),
    ('4','5','1.46'),
    ('4','6','1.36'),
    ('4','7','3.43'),
    ('4','8','3.39'),
    ('4','9','3'),
    ('4','10','1.71'),
    ('5','1','2.7'),
    ('5','2','1.66'),
    ('5','3','1.39'),
    ('5','4','4.61'),
    ('5','5','2.09'),
    ('5','6','4.79'),
    ('5','7','2.32'),
    ('5','8','1.45'),
    ('5','9','3.73'),
    ('5','10','1.06'),
    ('6','1','1.17'),
    ('6','2','4.28'),
    ('6','3','2.02'),
    ('6','4','1.24'),
    ('6','5','3.18'),
    ('6','6','3.38'),
    ('6','7','3.33'),
    ('6','8','1.87'),
    ('6','9','4.62'),
    ('6','10','2.61'),
    ('7','1','3.32'),
    ('7','2','3.98'),
    ('7','3','3.75'),
    ('7','4','1.68'),
    ('7','5','1.98'),
    ('7','6','4.3'),
    ('7','7','4.58'),
    ('7','8','4.65'),
    ('7','9','3.99'),
    ('7','10','2.17');

INSERT INTO student_can_shop (login, can_id, shop_id, purchase_time)
VALUES
    ('qcoelho', 7, 1, '2018-01-25 09:30'),
    ('tsitum', 7, 1, '2018-02-03 11:30'),
    ('atoubian', 7, 1, '2018-02-05 12:30'),
    ('qlhours', 7, 2, '2018-02-11 14:30'),
    ('ctresarr', 7, 1, '2018-02-04 16:30'),
    ('bteixeir', 7, 1, '2018-02-02 08:30'),
    ('nlayet', 7, 1, '2018-01-25 16:30'),
    ('rbillon', 7, 2, '2018-02-08 16:30'),
    ('aguiho', 7, 3, '2018-02-09 15:30'),
    ('spiat', 7, 1, '2018-02-07 16:30'),
    ('aou', 7, 4, '2018-02-02 17:20'),
    ('jdonnett', 7, 4, '2018-02-07 17:30'),
    ('cbrauge', 8, 1, '2018-02-02 17:40'),  -- 13
    ('pveyry', 8, 5, '2018-02-14 05:40'),
    ('cbrauge', 8, 6, '2018-02-12 19:40'),
    ('spiat', 7, 1, '2018-02-01 17:30'),    -- 16
    ('cbrauge', 8, 7, '2018-02-02 20:40'),
    ('pdagues', 8, 7, '2018-01-25 17:30'),
    ('wbekhtao', 8, 7, '2018-02-16 17:40'),
    ('nbarray', 8, 3, '2018-02-18 17:40');
