run:
	@docker-compose up

start:
	@docker-compose up -d

stop:
	@docker-compose stop

destroy:
	@docker-compose down

shell:
	@docker-compose exec db bash

dangerously-clean:
	# This will erase your databases and remove Docker data for good.
	# Recovery won't be possible at all!
	# To do this, execute the following commands by hand:
	@echo 'docker-compose down --rmi all -v'
	@echo 'rm -rf ./.docker/postgresql/*'
	@echo 'touch ./.docker/postgresql/.gitkeep'

.PHONY: run start stop destroy dangerously-clean shell
